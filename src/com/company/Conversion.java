package com.company;

/**
 * Created by july on 05/06/2017.
 */
public class Conversion {
    public static void main(String[] args) {

        int entero = 20;
        byte numb = 5;
        float numf = 10.5f;
        double numd = 200.2;

        double resultado;
        resultado= ((entero * numb)+numd)/numf;
        System.out.println("El resultado es: "+ resultado);

        // Casting
        int a = (int) numd;
        int b = (int) numf;
        byte c = (byte) numd;

        System.out.println(numd+" Cast a int " + a);
        System.out.println(numf+" Cast a int " + b);
        System.out.println(numd+" Cast a byte "+ c);

    }

}
